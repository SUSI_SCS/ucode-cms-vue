package xin.altitude.cms.plus.support;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
 * @author <a href="http://www.altitude.xin" target="_blank">赛泰先生</a>
 * @author <a href="https://gitee.com/decsa/ucode-cms-vue" target="_blank">UCode CMS</a>
 * @author <a href="https://space.bilibili.com/1936685014" target="_blank">B站视频</a>
 **/
public interface IBaseMapper<T> extends BaseMapper<T> {
    /**
     * 自增
     *
     * <pre>
     *     // 对学生表中主键为【1】的用户的【age】字段进行自增【1】
     *     studentMapper.incr(1L, "age", 1);
     * </pre>
     *
     * @param id    主键ID
     * @param field 需要自增的字段名
     * @param step  步长 步长需要大于0 否则会出现相反的业务逻辑
     * @return 如果更新成功返回1 否则返回0
     */
    int incr(@Param("pkVal") Serializable id, @Param("field") String field, @Param("step") int step);

    /**
     * 自增
     *
     * @param id    主键ID
     * @param field 需要自增的字段名
     * @param step  步长 步长需要大于0 否则会出现相反的业务逻辑
     * @return 如果更新成功返回1 否则返回0
     */
    int decr(@Param("pkVal") Serializable id, @Param("field") String field, @Param("step") int step);
}
